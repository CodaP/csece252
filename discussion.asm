	.ORIG x3000	
	LD   R0, START_R0
	LD   R1, START_R1
	LD   R2, START_R2
	LD   R3, START_R3
	LD   R4, START_R4
	LD   R5, START_R5
	LD   R6, START_R6
	LD   R7, START_R7	

; now call subroutine twice	
	LD   R2, ADDR1        	  ; load R2 with starting address of ASCII string (ASCII_to_Bin_data) before calling convertToBin
	JSR  convertToBin         ; call convertToBin subroutine

	ADD  R1, R0, #0 		  ; copy operand (written to R0 by subroutine) from R0 to R1
	
	LD   R2, ADDR2        	  ; load R2 with starting address of ASCII string (ASCII_to_Bin_data2) before calling convertToBin
	JSR  convertToBin         ; call convertToBin subroutine

	ADD  R2, R0, #0 		  ; copy operand from R0 to R2 (note: okay to use R2 because we no longer need to call subroutine)

; now do addition of the two operands
	ADD  R0, R1, R2			  ; put result of addition into R0 (this result should be x408A using the default values in the two data files)
	HALT					  ; done

	
START_R0		.FILL xAAAA
START_R1        .FILL x1111
START_R2		.FILL x2222
START_R3		.FILL x3333
START_R4		.FILL x4444
START_R5		.FILL x5555
START_R6		.FILL x6666
START_R7		.FILL x7777
	
ADDR1  			.FILL x3100
ADDR2  			.FILL x3110
