; This routine takes an ASCII string of decimal digits and converts it to an
; unsigned binary number, held in R0. The input ASCII string is located in consecutive
; memory locations starting at x3100. The end of the string is indicated by a null
; character, so the code can process strings of varying lengths.
;
; To perform the addition we will use the method where we convert digits from most-
; significant to least-significant, multiply our result by 10 each time before adding
; the next digit value. In other words, to convert "215", although we could compute
; 2*100 + 1*10 + 5*1, we factor out the multiplies as much as possible and instead
; compute ((2*10)+1)*10)+5
;
; The corresponding data files are ASCII_to_Bin_data.asm and ASCII_to_Bin_data2.asm
; (only use one of them at a time)

		.ORIG x3000
;;;;;;;;;;;;;;;;;;;;;;;;;;;;; (note: "ASCIItoBinLoop" is just a label for the start of this program)
ASCIItoBinLoop  AND  R0, R0, #0          ; initialize R0 to 0
		LD   R2, ASCIIaddress    ; R2 <- x3100 (start of ASCII string)
		LD   R3, ASCIIoffset     ; R3 <- xFFD0 (this is -x30)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; loop over all digits in the string, evaluating each digit, ;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; combining it into the total, and "shifting" the value to the ;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; left by one position before adding in the next digit value ;;;;;;;;;;

        LDR  R4, R2, #0          ; load R4 with the ASCII value of the first digit

LOOP	ADD  R4, R4, R3          ; convert digit ASCII code to binary value by subtracting 0x30
		ADD  R0, R0, R4          ; add this digit's contribution to R0

		LDR  R4, R2, #1          ; load R4 with the ASCII value in the *next* mem location
		BRz  DONE                ; if null char is in this next location (i.e. R4 = x0000), we're done (don't multiply by 10)
		                         ; if it is not null, we'll use that character it later (after we loop back)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;; "shift" value to left one decimal digit by multiplying by 10 ;;;;;
; Multiply the current value stored in R0 (let's call this value V) by 10.
; Instead of adding V to itself 10 times, we can optimize by using the
; formula  V*10 = V*(2+8) = (V*2) + (V*8)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		
		ADD  R0, R0, R0	         ; R0 <- V * 2 (save this value for later)
		ADD  R1, R0, R0	         ; R1 <- V * 4
		ADD  R1, R1, R1	         ; R1 <- V * 8
		ADD  R0, R0, R1	         ; R0 <- (V * 2) + (V * 8)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; REPEAT FOR NEXT CHARACTER ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		ADD  R2, R2, #1          ; increment R2 (i.e. "point" to next character in string)
		BRnzp  LOOP              ; unconditional branch back to start of loop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; DONE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DONE	HALT                     ; done with program 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; CONSTANT VALUES WE NEED IN MEMORY ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ASCIIoffset     .FILL xFFD0      ; xFFD0 is -x0030. x0030 is the ASCII code for '0'
ASCIIaddress    .FILL x3100		 ; this should be the start of the ASCII string
		.END
